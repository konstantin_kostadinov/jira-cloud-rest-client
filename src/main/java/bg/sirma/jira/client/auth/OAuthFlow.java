package bg.sirma.jira.client.auth;

public enum OAuthFlow {
    accessCode, implicit, password, application
}