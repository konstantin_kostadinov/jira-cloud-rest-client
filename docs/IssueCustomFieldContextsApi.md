# IssueCustomFieldContextsApi

All URIs are relative to *https://your-domain.atlassian.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addIssueTypesToContext**](IssueCustomFieldContextsApi.md#addIssueTypesToContext) | **PUT** /rest/api/3/field/{fieldId}/context/{contextId}/issuetype | Add issue types to context
[**assignProjectsToCustomFieldContext**](IssueCustomFieldContextsApi.md#assignProjectsToCustomFieldContext) | **PUT** /rest/api/3/field/{fieldId}/context/{contextId}/project | Assign custom field context to projects
[**createCustomFieldContext**](IssueCustomFieldContextsApi.md#createCustomFieldContext) | **POST** /rest/api/3/field/{fieldId}/context | Create custom field context
[**deleteCustomFieldContext**](IssueCustomFieldContextsApi.md#deleteCustomFieldContext) | **DELETE** /rest/api/3/field/{fieldId}/context/{contextId} | Delete custom field context
[**removeCustomFieldContextFromProjects**](IssueCustomFieldContextsApi.md#removeCustomFieldContextFromProjects) | **POST** /rest/api/3/field/{fieldId}/context/{contextId}/project/remove | Remove custom field context from projects
[**updateCustomFieldContext**](IssueCustomFieldContextsApi.md#updateCustomFieldContext) | **PUT** /rest/api/3/field/{fieldId}/context/{contextId} | Update custom field context

<a name="addIssueTypesToContext"></a>
# **addIssueTypesToContext**
> Object addIssueTypesToContext(body, fieldId, contextId)

Add issue types to context

Adds issue types to a custom field context, appending the issue types to the issue types list.  A custom field context without any issue types applies to all issue types. Adding issue types to such a custom field context would result in it applying to only the listed issue types.  If any of the issue types exists in the custom field context, the operation fails and no issue types are added.  **[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).

### Example
```java
// Import classes:
//import bg.sirma.jira.client.ApiClient;
//import bg.sirma.jira.client.ApiException;
//import bg.sirma.jira.client.Configuration;
//import bg.sirma.jira.client.auth.*;
//import bg.sirma.jira.client.api.IssueCustomFieldContextsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");
// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

IssueCustomFieldContextsApi apiInstance = new IssueCustomFieldContextsApi();
IssueTypeIds body = new IssueTypeIds(); // IssueTypeIds | 
String fieldId = "fieldId_example"; // String | The ID of the custom field.
Long contextId = 789L; // Long | The ID of the context.
try {
    Object result = apiInstance.addIssueTypesToContext(body, fieldId, contextId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssueCustomFieldContextsApi#addIssueTypesToContext");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**IssueTypeIds**](IssueTypeIds.md)|  |
 **fieldId** | **String**| The ID of the custom field. |
 **contextId** | **Long**| The ID of the context. |

### Return type

**Object**

### Authorization

[OAuth2](../README.md#OAuth2)[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="assignProjectsToCustomFieldContext"></a>
# **assignProjectsToCustomFieldContext**
> Object assignProjectsToCustomFieldContext(body, fieldId, contextId)

Assign custom field context to projects

Assigns a custom field context to projects.  If any project in the request is assigned to any context of the custom field, the operation fails.  **[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).

### Example
```java
// Import classes:
//import bg.sirma.jira.client.ApiClient;
//import bg.sirma.jira.client.ApiException;
//import bg.sirma.jira.client.Configuration;
//import bg.sirma.jira.client.auth.*;
//import bg.sirma.jira.client.api.IssueCustomFieldContextsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");
// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

IssueCustomFieldContextsApi apiInstance = new IssueCustomFieldContextsApi();
ProjectIds body = new ProjectIds(); // ProjectIds | 
String fieldId = "fieldId_example"; // String | The ID of the custom field.
Long contextId = 789L; // Long | The ID of the context.
try {
    Object result = apiInstance.assignProjectsToCustomFieldContext(body, fieldId, contextId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssueCustomFieldContextsApi#assignProjectsToCustomFieldContext");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectIds**](ProjectIds.md)|  |
 **fieldId** | **String**| The ID of the custom field. |
 **contextId** | **Long**| The ID of the context. |

### Return type

**Object**

### Authorization

[OAuth2](../README.md#OAuth2)[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createCustomFieldContext"></a>
# **createCustomFieldContext**
> CustomFieldContext createCustomFieldContext(body, fieldId)

Create custom field context

Creates a custom field context.  If &#x60;projectIds&#x60; is empty, a global context is created. A global context is one that applies to all project. If &#x60;issueTypeIds&#x60; is empty, the context applies to all issue types.  **[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).

### Example
```java
// Import classes:
//import bg.sirma.jira.client.ApiClient;
//import bg.sirma.jira.client.ApiException;
//import bg.sirma.jira.client.Configuration;
//import bg.sirma.jira.client.auth.*;
//import bg.sirma.jira.client.api.IssueCustomFieldContextsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");
// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

IssueCustomFieldContextsApi apiInstance = new IssueCustomFieldContextsApi();
CustomFieldContext body = new CustomFieldContext(); // CustomFieldContext | 
String fieldId = "fieldId_example"; // String | The ID of the custom field.
try {
    CustomFieldContext result = apiInstance.createCustomFieldContext(body, fieldId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssueCustomFieldContextsApi#createCustomFieldContext");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CustomFieldContext**](CustomFieldContext.md)|  |
 **fieldId** | **String**| The ID of the custom field. |

### Return type

[**CustomFieldContext**](CustomFieldContext.md)

### Authorization

[OAuth2](../README.md#OAuth2)[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteCustomFieldContext"></a>
# **deleteCustomFieldContext**
> Object deleteCustomFieldContext(fieldId, contextId)

Delete custom field context

Deletes a [ custom field context](https://confluence.atlassian.com/adminjiracloud/what-are-custom-field-contexts-991923859.html).  **[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).

### Example
```java
// Import classes:
//import bg.sirma.jira.client.ApiClient;
//import bg.sirma.jira.client.ApiException;
//import bg.sirma.jira.client.Configuration;
//import bg.sirma.jira.client.auth.*;
//import bg.sirma.jira.client.api.IssueCustomFieldContextsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");
// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

IssueCustomFieldContextsApi apiInstance = new IssueCustomFieldContextsApi();
String fieldId = "fieldId_example"; // String | The ID of the custom field.
Long contextId = 789L; // Long | The ID of the context.
try {
    Object result = apiInstance.deleteCustomFieldContext(fieldId, contextId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssueCustomFieldContextsApi#deleteCustomFieldContext");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fieldId** | **String**| The ID of the custom field. |
 **contextId** | **Long**| The ID of the context. |

### Return type

**Object**

### Authorization

[OAuth2](../README.md#OAuth2)[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="removeCustomFieldContextFromProjects"></a>
# **removeCustomFieldContextFromProjects**
> Object removeCustomFieldContextFromProjects(body, fieldId, contextId)

Remove custom field context from projects

Removes a custom field context from projects.  A custom field context without any projects applies to all projects. Removing all projects from a custom field context would result in it applying to all projects.  If any project in the request is not assigned to the context, or the operation would result in two global contexts for the field, the operation fails.  **[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).

### Example
```java
// Import classes:
//import bg.sirma.jira.client.ApiClient;
//import bg.sirma.jira.client.ApiException;
//import bg.sirma.jira.client.Configuration;
//import bg.sirma.jira.client.auth.*;
//import bg.sirma.jira.client.api.IssueCustomFieldContextsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");
// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

IssueCustomFieldContextsApi apiInstance = new IssueCustomFieldContextsApi();
ProjectIds body = new ProjectIds(); // ProjectIds | 
String fieldId = "fieldId_example"; // String | The ID of the custom field.
Long contextId = 789L; // Long | The ID of the context.
try {
    Object result = apiInstance.removeCustomFieldContextFromProjects(body, fieldId, contextId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssueCustomFieldContextsApi#removeCustomFieldContextFromProjects");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectIds**](ProjectIds.md)|  |
 **fieldId** | **String**| The ID of the custom field. |
 **contextId** | **Long**| The ID of the context. |

### Return type

**Object**

### Authorization

[OAuth2](../README.md#OAuth2)[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateCustomFieldContext"></a>
# **updateCustomFieldContext**
> Object updateCustomFieldContext(body, fieldId, contextId)

Update custom field context

Updates a [ custom field context](https://confluence.atlassian.com/adminjiracloud/what-are-custom-field-contexts-991923859.html).  **[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).

### Example
```java
// Import classes:
//import bg.sirma.jira.client.ApiClient;
//import bg.sirma.jira.client.ApiException;
//import bg.sirma.jira.client.Configuration;
//import bg.sirma.jira.client.auth.*;
//import bg.sirma.jira.client.api.IssueCustomFieldContextsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");
// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

IssueCustomFieldContextsApi apiInstance = new IssueCustomFieldContextsApi();
CustomFieldContextUpdateDetails body = new CustomFieldContextUpdateDetails(); // CustomFieldContextUpdateDetails | 
String fieldId = "fieldId_example"; // String | The ID of the custom field.
Long contextId = 789L; // Long | The ID of the context.
try {
    Object result = apiInstance.updateCustomFieldContext(body, fieldId, contextId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssueCustomFieldContextsApi#updateCustomFieldContext");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CustomFieldContextUpdateDetails**](CustomFieldContextUpdateDetails.md)|  |
 **fieldId** | **String**| The ID of the custom field. |
 **contextId** | **Long**| The ID of the context. |

### Return type

**Object**

### Authorization

[OAuth2](../README.md#OAuth2)[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

