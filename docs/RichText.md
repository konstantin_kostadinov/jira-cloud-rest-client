# RichText

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**valueSet** | **Boolean** |  |  [optional]
**emptyAdf** | **Boolean** |  |  [optional]
