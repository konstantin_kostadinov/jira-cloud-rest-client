# IncludedFields

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**excluded** | **List&lt;String&gt;** |  |  [optional]
**included** | **List&lt;String&gt;** |  |  [optional]
**actuallyIncluded** | **List&lt;String&gt;** |  |  [optional]
