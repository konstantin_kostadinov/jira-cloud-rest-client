# BulkIssuePropertyUpdateRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **Object** | The value of the property. The value must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters. |  [optional]
**filter** | **AllOfBulkIssuePropertyUpdateRequestFilter** | The bulk operation filter. |  [optional]
